import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Estagiario } from 'src/model/estagiario';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = 'http://spurbsp198/estagiario-teste/apiestagio.php/estagiario';

@Injectable({
  providedIn: 'root'
})
export class ApiEstagiarioService {

  constructor(private http: HttpClient) { }

  getEstagiarios (): Observable<Estagiario[]> {
    return this.http.get<Estagiario[]>(apiUrl)
      .pipe(
        //tap(estagiarios => console.log('leu os estags')),
        catchError(this.handleError('getEstagiarios', []))
      );
  }

  getEstagiario(id: number): Observable<Estagiario> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Estagiario>(url).pipe(
      catchError(this.handleError<Estagiario>(`getEstagiario id=${id}`))
    );
  }

  addEstagiario(estagiario): Observable<Estagiario> {
    return this.http.post<Estagiario>(apiUrl, estagiario, httpOptions).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      catchError(this.handleError<Estagiario>('addEstagiario'))
    );
  }

  updateEstagiario(id, estagiario): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, estagiario, httpOptions).pipe(
      catchError(this.handleError<any>('updateEstagiario'))
    );
  }

  deleteEstagiario(id): Observable<Estagiario> {
    const url = `${apiUrl}/${id}`;

    return this.http.delete<Estagiario>(url, httpOptions).pipe(
      catchError(this.handleError<Estagiario>('deleteEstagiario'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
