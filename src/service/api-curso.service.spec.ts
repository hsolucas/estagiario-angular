import { TestBed } from '@angular/core/testing';

import { ApiCursoService } from './api-curso.service';

describe('ApiCursoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApiCursoService = TestBed.get(ApiCursoService);
    expect(service).toBeTruthy();
  });
});
