import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Curso } from 'src/model/curso';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = 'http://spurbsp198/estagiario-teste/apiestagio.php/curso';

@Injectable({
  providedIn: 'root'
})
export class ApiCursoService {

  constructor(private http: HttpClient) { }

  getCursos (): Observable<Curso[]> {
    return this.http.get<Curso[]>(apiUrl)
      .pipe(
        catchError(this.handleError('getCursos', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
