import { TipoBeneficio } from './tipo-beneficio';

export class Beneficio {
  id: number;
  tipoBeneficio: TipoBeneficio;
  dias: number;
  valorDiaria: number;
  idContrato: number;
}
