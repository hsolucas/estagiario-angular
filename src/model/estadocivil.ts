export class EstadoCivil {
  constructor(_id: number, _nome: string) {
    this.id = _id;
    this.nome = _nome;
  }
  id: number;
  nome: string;
}
