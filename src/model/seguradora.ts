export class Seguradora {
  id: number;
  nome: string;
  apolice: string;
  ativo: number;
}
