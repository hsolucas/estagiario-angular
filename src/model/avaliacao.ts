import { RespostaAval } from './resposta-aval';

export class Avaliacao {
  id: number;
  respEmpresa: string;
  dtAvaliacao: Date;
  dtCad: Date;
  idContrato: number;
  nome: string;
  respostas: RespostaAval[];
}
