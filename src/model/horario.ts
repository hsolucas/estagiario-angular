export class Horario {
  id: number;
  idContrato: number;
  hrEntrada: string;
  hrSaida: string;
  dia: number;
}
