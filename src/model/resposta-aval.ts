export class RespostaAval {
  id: number;
  ordemQuestao: number;
  resposta: string;
  dtCad: Date;
  idAvaliacao: number;
}
