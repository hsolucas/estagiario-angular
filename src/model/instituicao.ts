export class Instituicao {
  id: number;
  nome: string;
  endereco: string;
  telefone: string;
  email: string;
  campus: string;
}
