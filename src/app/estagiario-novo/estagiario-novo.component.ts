import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ApiEstagiarioService } from 'src/service/api.service';

import { EstadoCivil } from 'src/model/estadocivil';

@Component({
  selector: 'app-estagiario-novo',
  templateUrl: './estagiario-novo.component.html',
  styleUrls: ['./estagiario-novo.component.css']
})
export class EstagiarioNovoComponent implements OnInit {
  estagForm: FormGroup;
  constructor(private router: Router, private api: ApiEstagiarioService, private formBuilder: FormBuilder) { }

  listaEstCivil: EstadoCivil[] = [
    new EstadoCivil(1, 'Solteiro'),
    new EstadoCivil(2, 'Casado')
  ];

  ngOnInit() {

     this.estagForm = this.formBuilder.group({
      'nome' : [null, Validators.required],
      'matricula' : [null, [Validators.required, Validators.minLength(2)]],
      'documento' : [null, Validators.required],
      'dataExpDoc' : [null],
      'cpf' : [null, Validators.required],
      'nacionalidade' : [null, Validators.required],
      'naturalidade' : [null],
      'dataNasc' : [null, Validators.required],
      'estCivil' : [null, Validators.required],
      'cep' : [null, Validators.required],
      'endereco' : [null, Validators.required],
      'numeroEndereco' : [null, Validators.required],
      'compl' : [null, null],
      'telefone' : [null, null],
      'email' : [null, null],
    });

  }

  addEstagiario(form: NgForm) {
    this.api.addEstagiario(form)
      .subscribe(res => {
          //const id = res['_id'];
          const id = res;
          console.log("ID  criado " + id);
          this.router.navigate(['/estagiario-detalhe', id]);
        }, (err) => {
          console.log(err);
        });
  }

}
