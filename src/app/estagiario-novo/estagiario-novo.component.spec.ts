import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstagiarioNovoComponent } from './estagiario-novo.component';

describe('EstagiarioNovoComponent', () => {
  let component: EstagiarioNovoComponent;
  let fixture: ComponentFixture<EstagiarioNovoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstagiarioNovoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstagiarioNovoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
