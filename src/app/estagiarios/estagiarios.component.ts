import { Component, OnInit, ViewChild } from '@angular/core';

import { ApiEstagiarioService } from 'src/service/api.service';
import { ApiCursoService } from 'src/service/api-curso.service';

import { Estagiario } from 'src/model/estagiario';
import { Curso } from 'src/model/curso';
import { EstadoCivil } from 'src/model/estadocivil';

import { AppComponent } from 'src/app/app.component';

import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-estagiarios',
  templateUrl: './estagiarios.component.html',
  styleUrls: ['./estagiarios.component.css']
})
export class EstagiariosComponent implements OnInit {

  displayedColumns: string[] = ['nome', 'ren', 'aval', 'plano', 'rece', 'benef', 'obs'];

  filtro = {nome: '', curso: 0, area: ''};

  listaEstag: Estagiario[];
  listaCurso: Curso[];

  dataSource = new MatTableDataSource<Estagiario>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  private subscription: any;

  constructor(private api: ApiEstagiarioService, private apiCurso: ApiCursoService, private master: AppComponent) { }

  ngOnInit() {
    this.master.isLoading = true;

    this.subscription =
      this.api.getEstagiarios().subscribe(
        res => {
          this.listaEstag = res;
          this.setDataSource(this.listaEstag);
        },
        err => {
          this.master.showAlert(`Erro ao consultar os estagiários. ${err}`);
        },
        () => { //finally
          this.master.isLoading = false;
        }
      )


    this.apiCurso.getCursos().subscribe(
        res => {
          this.listaCurso = res;
          //this.setDataSource(this.listaEstag);
        },
        err => {
          this.master.showAlert(`Erro ao consultar os cursos. ${err}`);
        }
    );
  }

  ngOnDestroy(){
    console.log("Destroy");
    this.subscription.unsubscribe();
  }

  setDataSource(lista: Estagiario[]){
    this.dataSource.data = lista;
    this.dataSource.paginator = this.paginator;
    this.dataSource.paginator.hidePageSize = true;
    this.master.showAlert(`${lista.length} estagiário(s) encontrado(s)`);
  }


  consultaEstagiario() {
    let lista : Estagiario[] = [];
    for(let e of this.listaEstag){
      let fNome = this.filtro.nome ? e.nome.indexOf(this.filtro.nome) >= 0 : true;
      let fCurso;
      for(let ct of e.contratos){
        if(ct.curso.id == this.filtro.curso){
          fCurso = true;
          break;
        }
        fCurso = false;
      }

      if(fNome && fCurso){
        lista.push(e);
      }
    }
    this.setDataSource(lista);
  }

    diasRestantes(estag: Estagiario) {
      if (estag.ativo) {
          for (const contr of estag.contratos) {
            const objDtFim = new Date(contr.dtFimContrato);
            const diasRest = (objDtFim.getTime() - new Date().getTime())/86400000;
            return Math.floor(diasRest);
          }
      }
      return -1;
    }

}
