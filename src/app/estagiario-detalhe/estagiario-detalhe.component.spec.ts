import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstagiarioDetalheComponent } from './estagiario-detalhe.component';

describe('EstagiarioDetalheComponent', () => {
  let component: EstagiarioDetalheComponent;
  let fixture: ComponentFixture<EstagiarioDetalheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstagiarioDetalheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstagiarioDetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
