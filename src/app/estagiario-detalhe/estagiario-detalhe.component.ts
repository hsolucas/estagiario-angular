import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiEstagiarioService } from 'src/service/api.service';
import { Estagiario } from 'src/model/estagiario';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-estagiario-detalhe',
  templateUrl: './estagiario-detalhe.component.html',
  styleUrls: ['./estagiario-detalhe.component.css']
})
export class EstagiarioDetalheComponent implements OnInit {
  estagiario: Estagiario = new Estagiario();
  isLoadingResults = true;
  constructor(private router: Router, private route: ActivatedRoute, private api: ApiEstagiarioService, private master: AppComponent) { }

  ngOnInit() {
    this.getEstagiario(this.route.snapshot.params['id']);
  }

  getEstagiario(id) {
    this.api.getEstagiario(id)
      .subscribe(data => {
        this.estagiario = data;
        this.isLoadingResults = false;
      });
  }

  deleteEstagiario(id) {
    this.isLoadingResults = true;
    this.api.deleteEstagiario(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['/estagiarios']);
        }, (err) => {
          this.master.showAlert(`Erro ao acessar os dados. ${err}`);
          this.isLoadingResults = false;
        }
      );
  }

}
