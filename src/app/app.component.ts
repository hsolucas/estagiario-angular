//import { Component } from '@angular/core';
import { Component, Injectable } from '@angular/core';
import { MatSnackBar } from "@angular/material";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-estagiario';

  constructor(public snackBar: MatSnackBar) {}

  isLoading = true;

   showAlert(message: string) {
     let action = 'Fechar';
      this.snackBar.open(message, action, {
         duration: 3000,
      });
   }

}
