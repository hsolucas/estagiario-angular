import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EstagiariosComponent } from './estagiarios/estagiarios.component';
import { EstagiarioDetalheComponent } from './estagiario-detalhe/estagiario-detalhe.component';
import { EstagiarioNovoComponent } from './estagiario-novo/estagiario-novo.component';
import { EstagiarioEditarComponent } from './estagiario-editar/estagiario-editar.component';


const routes: Routes = [
  {
    path: 'estagiarios',
    component: EstagiariosComponent,
    data: { title: 'Lista de Estags' }
  },
  {
    path: 'estagiario-detalhe/:id',
    component: EstagiarioDetalheComponent,
    data: { title: 'Detalhe do Est' }
  },
  {
    path: 'estagiario-novo',
    component: EstagiarioNovoComponent,
    data: { title: 'Adicionar Estag' }
  },
  {
    path: 'estagiario-editar/:id',
    component: EstagiarioEditarComponent,
    data: { title: 'Editar o Estag' }
  },
  { path: '',
    redirectTo: '/estagiarios',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
