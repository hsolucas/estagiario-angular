/* Principais do Core  */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

/* Forms */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';

/* Rotas */
import { AppRoutingModule } from './app-routing.module';

/* Components */
import { AppComponent } from './app.component';
import { EstagiariosComponent } from './estagiarios/estagiarios.component';
import { EstagiarioDetalheComponent } from './estagiario-detalhe/estagiario-detalhe.component';
import { EstagiarioNovoComponent } from './estagiario-novo/estagiario-novo.component';
import { EstagiarioEditarComponent } from './estagiario-editar/estagiario-editar.component';

/* Angular Material - Layout */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './menu/menu.component';
import {
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSidenavModule,
  MatTableModule,
  MatToolbarModule,
  MatSnackBarModule } from '@angular/material';

import {MatPaginatorModule} from '@angular/material/paginator';

import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';

import { MatGridListModule } from '@angular/material/grid-list';


@NgModule({
  declarations: [
    AppComponent,
    EstagiariosComponent,
    EstagiarioDetalheComponent,
    EstagiarioNovoComponent,
    EstagiarioEditarComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,MatSnackBarModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSidenavModule,
    MatTableModule,
    MatToolbarModule,
    LayoutModule,
    MatPaginatorModule,
    NgSelectModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
