import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ApiEstagiarioService } from 'src/service/api.service';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-estagiario-editar',
  templateUrl: './estagiario-editar.component.html',
  styleUrls: ['./estagiario-editar.component.css']
})
export class EstagiarioEditarComponent implements OnInit {
  id: number = 0;
  estagForm: FormGroup;
  nome: String = '';
  matricula: String = '';
  documento: number = null;
  isLoadingResults = false;

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiEstagiarioService, private formBuilder: FormBuilder, private master: AppComponent) { }

  ngOnInit() {
    this.getEstagiario(this.route.snapshot.params['id']);
    this.estagForm = this.formBuilder.group({
     'nome' : [null, Validators.required],
     'matricula' : [null, Validators.required],
     'documento' : [null, Validators.required]
    });

  }

   getEstagiario(id) {
    this.api.getEstagiario(id).subscribe(data => {
      this.id = data.id;
      this.estagForm.setValue({
        nome: data.nome,
        matricula: data.matricula,
        documento: data.documento
      });
    });
  }

  updateEstagiario(form: NgForm) {
    this.isLoadingResults = true;
    this.api.updateEstagiario(this.id, form)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['/estagiario-detalhe/' + this.id]);
        }, (err) => {
          this.master.showAlert(`Erro ao acessar os dados. ${err}`);
          this.isLoadingResults = false;
        }
      );
  }

}
