import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstagiarioEditarComponent } from './estagiario-editar.component';

describe('EstagiarioEditarComponent', () => {
  let component: EstagiarioEditarComponent;
  let fixture: ComponentFixture<EstagiarioEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstagiarioEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstagiarioEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
